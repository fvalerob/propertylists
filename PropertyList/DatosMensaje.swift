//
//  DatosMensaje.swift
//  PropertyList
//
//  Created by Fran on 8/1/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import Foundation
struct DatosMensaje: Codable{
    var mensaje: String!
    var fechaEdicion: String!
}
