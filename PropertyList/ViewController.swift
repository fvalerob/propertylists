//
//  ViewController.swift
//  PropertyList
//
//  Created by Fran on 8/1/18.
//  Copyright © 2018 Fran. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var mensaje: UITextView!
    var fechaEdicion: Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view,
        self.mensaje.delegate = self;
        let nc = NotificationCenter.default
        nc.addObserver(self, selector:#selector(self.vamosABackground), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        var urlDocs = FileManager.default.urls(for:.documentDirectory,
                                               in:.userDomainMask)[0]
        let urlPlist = urlDocs.appendingPathComponent("mensaje.plist")
        
        do{
            let data = try Data(contentsOf: urlPlist)
                let decoder = PropertyListDecoder()
                let misProps = try decoder.decode(DatosMensaje.self, from: data)
                print(misProps)
            if(misProps.fechaEdicion != "" && misProps.mensaje != ""){
                self.mensaje.text = misProps.mensaje
                self.fechaLabel.text = misProps.fechaEdicion
            }
        }catch{}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesEnded(_ touches: Set<UITouch>, with: UIEvent?) {
        //CUIDADO: estamos suponiendo que el outlet del text view se llama "mensaje"
        //cámbialo por como lo hayas llamado tú
        self.mensaje.resignFirstResponder()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        self.fechaEdicion = Date()
        //SUPONEMOS que el outlet del label se llama "fechaLabel"
        //CAMBIALO si es necesario
        self.fechaLabel.text = DateFormatter.localizedString(
            from:self.fechaEdicion!,
            dateStyle: .short, timeStyle: .medium)
    }
    @objc func vamosABackground() {
        print("Nos notifican que vamos a background");
        var urlDocs = FileManager.default.urls(for:.documentDirectory,
                                               in:.userDomainMask)[0]
        let urlPlist = urlDocs.appendingPathComponent("mensaje.plist")
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        let misProps = DatosMensaje(mensaje:self.mensaje.text, fechaEdicion:self.fechaLabel.text)
        do{
            let data = try encoder.encode(misProps)
            try data.write(to: urlPlist)
        }catch{
            print("Error al crear el fichero.");
        }
    }
    
    
}

